python-testfixtures (6.14.1-2) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andrey Rahmatullin ]
  * Drop B-D: python3-mock (Closes: #980138).

 -- Ondřej Nový <onovy@debian.org>  Thu, 24 Sep 2020 08:51:37 +0200

python-testfixtures (6.14.1-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Andrey Rahmatullin ]
  * New upstream version.
  * Add an upstream fix for Python 3.8.3 (Closes: #963369).

 -- Andrey Rahmatullin <wrar@debian.org>  Mon, 29 Jun 2020 21:07:55 +0500

python-testfixtures (6.10.1-1) unstable; urgency=medium

  * Team upload.

  [ Michael Fladischer ]
  * Bump debhelper version to 12.
  * Bump Standards-Version to 4.4.1.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Add debian/gbp.conf.

  [ Andrey Rahmatullin ]
  * New upstream version.
  * Remove external image links from README.rst.
  * Add Built-Using: ${sphinxdoc:Built-Using}.
  * Add Rules-Requires-Root: no.

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 13 Nov 2019 20:15:05 +0500

python-testfixtures (4.14.3-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support.

 -- Andrey Rahmatullin <wrar@debian.org>  Fri, 16 Aug 2019 00:50:38 +0500

python-testfixtures (4.14.3-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Enable autopkgtest-pkg-python testsuite.
  * d/control: Set Vcs-* to salsa.debian.org.
  * d/control: Remove ancient X-Python-Version field.
  * d/control: Remove ancient X-Python3-Version field.
  * Convert git repository from git-dpm to gbp layout.
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs.

  [ James Valleroy ]
  * Add patch to deal with the comma vanishing from repr in 3.7
    (Closes: #912184).
  * Update debhelper compat level to 11.
  * Change doc-base Index and Files to match file locations.
  * Standards-Version is 4.2.1 now (no changes needed).

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 19 Dec 2018 11:21:01 +0100

python-testfixtures (4.14.3-1) unstable; urgency=low

  * New upstream release.
  * Add Django support to documentation and tests.

 -- Michael Fladischer <fladi@debian.org>  Wed, 31 May 2017 11:27:41 +0200

python-testfixtures (4.13.5-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Thu, 16 Mar 2017 12:00:41 +0100

python-testfixtures (4.13.4-1) unstable; urgency=low

  * New upstream release (Closes: #853912).
  * Remove unused lintian overrides.

 -- Michael Fladischer <fladi@debian.org>  Fri, 17 Feb 2017 11:20:07 +0100

python-testfixtures (4.13.3-1) unstable; urgency=low

  * Initial release (Closes: #852783).

 -- Michael Fladischer <fladi@debian.org>  Thu, 26 Jan 2017 17:34:20 +0100
